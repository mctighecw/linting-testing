# README

Mini project to use some linting and testing.

## App Information

App Name: linting-testing

Created: March 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/linting-testing)

## Tech Stack

* React hooks
* Webpack
* Sass (using CSS modules)
* ESLint
* Prettier
* Jest
* Enzyme

## To Set Up and Run

1. Install Node modules and start frontend

  ```
  $ npm install
  $ npm start
  ```

2. Run tests (Jest and Enzyme)

  ```
  $ npm run test
  ```

3. Set up linting

  A. Set up ESLint in Atom editor

  Install the following Atom packages:

  - linter
  - linter-eslint
  - linter-ui-default

  _OR_

  B. Install _prettier-atom_ in Atom editor

Last updated: 2024-09-04
