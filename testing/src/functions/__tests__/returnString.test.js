import { returnString } from "functions/returns";

test("returns a string", () => {
  const result = returnString("testing");
  expect(typeof result).toBe("string");
});
