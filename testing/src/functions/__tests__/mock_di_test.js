// See https://medium.com/@rickhanlonii/understanding-jest-mocks-f0046c68e53c

const doMultiplication = (a, b, callback) => {
  callback(a * b);
};

test("calls callback with arguments added", () => {
  const mockCallback = jest.fn();
  doMultiplication(3, 4, mockCallback);
  expect(mockCallback).toHaveBeenCalledWith(12);
});
