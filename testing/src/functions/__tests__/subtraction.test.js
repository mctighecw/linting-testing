import { subtraction } from "functions/math";

test("subtracts 6 minus 3 to equal 3", () => {
  expect(subtraction(6, 3)).toBe(3);
});
