import { timesSeven } from "functions/math";

test("multiplies 5 times 7 to equal 35", () => {
  expect(timesSeven(5)).toBe(35);
});
