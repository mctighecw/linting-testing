import { addition } from "functions/math";

test("adds 4 and 5 to equal 9", () => {
  expect(addition(4, 5)).toBe(9);
});
