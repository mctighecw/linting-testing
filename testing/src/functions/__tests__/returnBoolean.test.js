import { returnBoolean } from "functions/returns";

test("returns a boolean value", () => {
  const result = returnBoolean();
  expect(typeof result).toBe("boolean");
});
