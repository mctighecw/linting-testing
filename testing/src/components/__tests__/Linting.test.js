import { shallow } from 'enzyme';
import React from 'react';
import Linting from 'components/Linting/Linting';

describe('Linting component rendering', () => {
  it('should render without crashing', () => {
    expect(() => {
      shallow(<Linting />);
    })
      .not.toThrow();
  });
});
