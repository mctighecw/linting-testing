import { shallow } from 'enzyme';
import React from 'react';
import Main from 'components/Main/Main';

describe('Main component rendering', () => {
  it('should render without crashing', () => {
    expect(() => {
      shallow(<Main />);
    })
      .not.toThrow();
  });
});
