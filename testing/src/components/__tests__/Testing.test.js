import { shallow } from 'enzyme';
import React from 'react';
import Testing from 'components/Testing/Testing';

describe('Testing component rendering', () => {
  it('should render without crashing', () => {
    expect(() => {
      shallow(<Testing />);
    })
      .not.toThrow();
  });
});
