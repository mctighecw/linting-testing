import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Link from "components/shared/Link/Link";

describe('Link', () => {
  it('should render correctly', () => {
    const output = shallow(
      <Link
        url="mockUrl"
        label="mockLabel"
        style="dashed"
      />
    );
    expect(shallowToJson(output)).toMatchSnapshot();
  });
});
