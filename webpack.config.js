const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const NamedModulesPlugin = require("name-all-modules-plugin");

module.exports = {
  mode: "development",
  entry: __dirname + "/src/index.js",
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "build"),
    libraryTarget: "umd",
    publicPath: "/"
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      assets: path.resolve(__dirname, "src/assets"),
      components: path.resolve(__dirname, "src/components"),
      functions: path.resolve(__dirname, "src/functions"),
      styles: path.resolve(__dirname, "src/styles"),
      "SassConstants": path.resolve(__dirname, 'src/styles/constants.scss')
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.ico$/,
        use: "file-loader?name=assets/[name].[ext]"
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: "css-loader"
        })
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          "css-loader?modules&localIdentName=[hash:base64:6]",
          "sass-loader"
        ]
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.pdf$/,
        use: "file-loader?name=assets/[name].[ext]"
      },
      {
        test: /\.(ttf|otf|eot)$/,
        use: "file-loader?name=assets/[name].[ext]"
      },
      {
        test: /\.(woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: "file-loader?limit=10000&name=assets/[name].[ext]"
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("development")
      }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new HTMLWebpackPlugin({
      template: path.resolve(__dirname, "src/index.html"),
      filename: "index.html",
      inject: "body",
      favicon: "src/assets/favicon.ico"
    }),
    new ExtractTextPlugin("stylesheet.css")
  ],
  devtool: "eval",
  devServer: {
    port: "3000",
    historyApiFallback: true,
    hot: true
  }
};
