module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    "testing/src**",
    "testing/components**",
    "testing/functions**",
    "!**/__tests__/**",
    "!**/__mocks__/**"
  ],
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/testing/src/__mocks__/fileMock.js",
    "\\.(css|less|sass|scss)$": "<rootDir>/testing/src/__mocks__/styleMock.js"
  },
  modulePaths: ["src"],
  coverageDirectory: "coverage_reports",
  coverageReporters: ["text", "html"],
  snapshotSerializers: ["enzyme-to-json/serializer"],
  setupFilesAfterEnv: ["<rootDir>/jest.setup.js", "<rootDir>/enzyme.config.js"]
};
