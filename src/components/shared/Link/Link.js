import React from "react";
import "./styles.scss";

const Link = ({ url, style, label }) => (
  <a href={url} target="_blank" styleName={style}>
    {label}
  </a>
);

export default Link;
