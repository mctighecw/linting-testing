import React from "react";
import Link from "components/shared/Link/Link";
import "./styles.scss";

const Linting = () => (
  <div styleName="linting">
    <div styleName="blockquote">
      <p>
        ESLint is an open source JavaScript linting utility originally created
        by Nicholas C. Zakas in June 2013. Code linting is a type of static
        analysis that is frequently used to find problematic patterns or code
        that doesn’t adhere to certain style guidelines. There are code linters
        for most programming languages, and compilers sometimes incorporate
        linting into the compilation process.
      </p>
      <p>
        JavaScript, being a dynamic and loosely-typed language, is especially
        prone to developer error. Without the benefit of a compilation process,
        JavaScript code is typically executed in order to find syntax or other
        errors. Linting tools like ESLint allow developers to discover problems
        with their JavaScript code without executing it.
      </p>
      <p>
        The primary reason ESLint was created was to allow developers to create
        their own linting rules. ESLint is designed to have all rules completely
        pluggable. The default rules are written just like any plugin rules
        would be. They can all follow the same pattern, both for the rules
        themselves as well as tests. While ESLint will ship with some built-in
        rules to make it useful from the start, you’ll be able to dynamically
        load rules at any point in time.
      </p>
      <p>
        ESLint is written using Node.js to provide a fast runtime environment
        and easy installation via npm.
      </p>
      <div styleName="citation">
        <Link
          url="https://eslint.org/docs/about/"
          style="red"
          label="ESLint.org"
        />
      </div>
    </div>

    <div styleName="blockquote">
      <p>
        Prettier is an opinionated code formatter with support for [various web
        technologies].
      </p>
      <p>
        It removes all original styling* and ensures that all outputted code
        conforms to a consistent style.
      </p>
      <div styleName="citation">
        <Link
          url="https://prettier.io/docs/en/index.html"
          style="red"
          label="Prettier.io"
        />
      </div>
    </div>
  </div>
);

export default Linting;
