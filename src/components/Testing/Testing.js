import React from "react";
import Link from "components/shared/Link/Link";
import "./styles.scss";

import JestLogo from "assets/jest_logo.png";
import EnzymeLogo from "assets/enzyme_logo.png";
import JasmineLogo from "assets/jasmine_logo.png";
import MochaLogo from "assets/mocha_logo.svg";

const Testing = () => (
  <div styleName="testing">
    <div styleName="heading">
      There are various libraries for testing JavaScript applications.
      <br />
      Here are some popular ones:
    </div>

    <div styleName="libary">
      <div styleName="info">
        <div styleName="name">Jest</div>
        <div styleName="description">
          "Jest is a delightful JavaScript Testing Framework with a focus on
          simplicity. It works with projects using: Babel, TypeScript, Node,
          React, Angular, Vue and more!"
        </div>
        <Link
          url="https://jestjs.io/"
          style="dashed"
          label="Jest site"
        />
      </div>
      <img
        src={JestLogo}
        alt="Jest logo"
        onClick={() => window.open("https://jestjs.io/")}
      />
    </div>

    <div styleName="border" />

    <div styleName="libary">
      <img
        src={EnzymeLogo}
        alt="Enzyme logo"
        onClick={() => window.open("https://airbnb.io/enzyme/")}
      />
      <div styleName="info">
        <div styleName="name">Enzyme</div>
        <div styleName="description">
          "Enzyme is a JavaScript Testing utility for React that makes it easier
          to test your React Components' output. You can also manipulate,
          traverse, and in some ways simulate runtime given the output."
        </div>
        <Link
          url="https://airbnb.io/enzyme/"
          style="dashed"
          label="Enzyme site"
        />
      </div>
    </div>

    <div styleName="border" />

    <div styleName="libary">
      <div styleName="info">
        <div styleName="name">Jasmine</div>
        <div styleName="description">
          "Jasmine is a behavior-driven development framework for testing
          JavaScript code. It does not depend on any other JavaScript
          frameworks. It does not require a DOM. And it has a clean, obvious
          syntax so that you can easily write tests."
        </div>
        <Link
          url="https://jasmine.github.io/"
          style="dashed"
          label="Jasmine site"
        />
      </div>
      <img
        src={JasmineLogo}
        alt="Jasmine logo"
        onClick={() => window.open("https://jasmine.github.io/")}
      />
    </div>

    <div styleName="border" />

    <div styleName="libary">
      <img
        src={MochaLogo}
        alt="Mocha logo"
        onClick={() => window.open("https://mochajs.org/")}
      />
      <div styleName="info">
        <div styleName="name">Mocha</div>
        <div styleName="description">
          "Mocha is a feature-rich JavaScript test framework running on Node.js
          and in the browser, making asynchronous testing simple and fun. Mocha
          tests run serially, allowing for flexible and accurate reporting,
          while mapping uncaught exceptions to the correct test cases."
        </div>
        <Link
          url="https://mochajs.org/"
          style="dashed"
          label="Mocha site"
        />
      </div>
    </div>
  </div>
);

export default Testing;
