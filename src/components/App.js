import React from "react";
import Main from "components/Main/Main";
import "../styles/global.css";

const App = () => (
  <Main />
);

export default App;
