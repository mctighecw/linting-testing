import React, { useState } from "react";
import CodeIcon from "assets/code-icon.png";
import Linting from "components/Linting/Linting";
import Testing from "components/Testing/Testing";
import "./styles.scss";

const Main = () => {
  const [showLinting, setLinting] = useState(false);
  const [showTesting, setTesting] = useState(false);

  const toggleShowTopic = topic => {
    if (topic === "linting") {
      setTesting(false);
      setLinting(true);
    } else {
      setLinting(false);
      setTesting(true);
    }
  };

  return (
    <div styleName="main">
      <div styleName="heading">Clean JavaScript</div>

      <div styleName="sub-heading">
        Learn more about{" "}
        <span onClick={() => toggleShowTopic("linting")}>linting</span> or{" "}
        <span onClick={() => toggleShowTopic("testing")}>testing</span>.
      </div>

      <div styleName="topics">
        {!showLinting && !showTesting && <img src={CodeIcon} alt="Code icon" />}
        {showLinting && <Linting />}
        {showTesting && <Testing />}
      </div>
    </div>
  );
};

export default Main;
