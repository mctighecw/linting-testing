// -- Setup code to be run before each test suite goes here -- //

// Add matcher to assert valid numeric values
expect.extend({
  toBeValidNumber(received, argument) {
    if (received === null || received === undefined) {
      return {
        pass: false,
        message: () =>
          `Value expected to be valid number. Value is ${received}.`
      };
    } else if (typeof received !== "number") {
      return {
        pass: false,
        message: () =>
          `Value expected to be valid number. typeof ${received} is ${typeof received}.)`
      };
    } else if (Number.isNaN(received)) {
      return {
        pass: false,
        message: () => "Value expected to be valid number. It is NaN instead."
      };
    }
    return {
      pass: true,
      message: () => "Value expected NOT to be valid number, but it is."
    };
  }
});

// Make console warnings and errors fail tests
console.warn = (...args) => {
  const error = args.join('\n');
  throw Error(error);
};

console.error = (...args) => {
  const error = args.join('\n');
  throw Error(error);
};
